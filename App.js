import React,{useState} from 'react';
import {
  FlatList,
  Linking,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { Searchbar } from 'react-native-paper';
import axios from "axios";

const App =() =>  {

  const [search, setSearch] = useState('');
  const [data,setData]=useState([]);
  const [error, setError] = useState('');

  const fetchData =async()=>{
  if(search===" ") return
   await axios.get(`https://google-search3.p.rapidapi.com/api/v1/search/q=${search}&num=100`,{
        headers: {
          'x-rapidapi-key': '30a79d49e4msh60149164f98a8a5p16accdjsn3726a274f766',
          'x-rapidapi-host':'google-search3.p.rapidapi.com',
          'useQueryString':'true'
        }
       }).then(res=>{
         {
          setData(res.data.results)
         }
       }).catch((error)=>setError("No results found")) 
  }
  
  return (
   <View style={styles.container}>
   
    <Searchbar
        placeholder="Type here"
        onChangeText={(text)=>setSearch(text)}
        value={search}
        style={{width:"80%"}}
    />

    <TouchableOpacity  style={styles.buttonStyle} onPress={fetchData} >
      <Text style={styles.buttonTextStyle}>Search</Text>
    </TouchableOpacity> 

     { error ? <Text style={styles.errorStyle}>No results found</Text>
        :(
          <FlatList
            data={data}
            keyExtractor={(item,index)=> index}
            renderItem={({item}) =>
             <TouchableOpacity style={{marginTop:10}} onPress={()=>Linking.openURL(item.link)}>
                <Text style={styles.textStyle}>{item.link}</Text>
             </TouchableOpacity>}
        />
        )
        }
   </View>
  );
};

const styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
    marginTop:40
  },
  textStyle:{
    borderWidth:.5,
    textAlign:'center',
    fontSize:20,
    marginHorizontal:20,
    color:"blue"
},
errorStyle:{
  color:'red',
  paddingVertical:10,
  marginTop:10
},
buttonStyle:{
  justifyContent:'center',
  alignItems:'center',
  backgroundColor:'steelblue',
  width:"30%",
  height:30,
  marginVertical:20,
  borderRadius:20
},
buttonTextStyle:{
color:'#fff',
fontSize:16
}
});

export default App;
